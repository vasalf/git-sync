#!/bin/bash

# sync-daemon.sh: syncronize your git repository
# Copyright (c) 2017 Vasily Alferov /platypus/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Usage:
# 0. Copy this script to your project root.
# 1. Add sync-daemon.sh, nohup.out and sync-logs/* to your gitignore.
# 2. Set the SYNC_INTERVAL (in seconds).
# 3. Write build function.
# 4. Launch with:
#       nohup ./sync-daemon.sh &

SYNC_INTERVAL=30

function build() {
    true
}

function update() {
    fetch_output=$(git fetch --dry-run 2>&1)
    fetch_result=$?
    if [[ $fetch_result -ne 0 ]]; then
        echo $(date): fetch returned $fetch_result, aborting update >> sync-logs/sync.log
        return 1
    fi
    if [[ $fetch_output ]]; then
        echo "=== $(date): performed fetch (result $fetch_result)" === >> sync-logs/fetch.log
        echo "$fetch_output" >> sync-logs/fetch.log
        echo "$(date): performing pull" >> sync-logs/sync.log
        echo "=== $(date): performing pull ===" >> sync-logs/pull.log
        git pull >> sync-logs/pull.log
        pull_result=$?
        echo "$(date): pull returned $pull_result" >> sync-logs/sync.log
        echo "=== $(date): performing build ===" >> sync-logs/build.log
        build >> sync-logs/build.log
        build_result=$?
        echo "$(date): build returned $build_result" >> sync-logs/sync.log
    fi
}

if [ ! -d sync-logs/ ]; then
    mkdir -p sync-logs/
fi
while true; do
    update
    sleep $SYNC_INTERVAL
done
